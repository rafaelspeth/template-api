//const express = require("express");

function validateUserBody (req, res, next) {
  const baseMessage = "Request body had malformed field"
  // check if all fields are defined
  console.log(req.body);
  if (!req.body.name) {
    return res.status(400).send({ error: baseMessage + " name" });
  }
  if (!req.body.email) {
    return res.status(400).send({ error: baseMessage + " email" });
  }
  if (!req.body.password) {
    return res.status(400).send({ error: baseMessage + " password" });
  }
  if (!req.body.passwordConfirmation) {
    return res.status(400).send({ error: baseMessage + " passwordConfirmation" });
  }
  // check if name is valid
  if (!/^[a-zA-Z][^\s]*$/.test(req.body.name)) {
    return res.status(400).send({ error: baseMessage + " name" })
  }
  // check if password is valid
  if (!/^[\d\w]{8,32}$/.test(req.body.password)) {
    return res.status(400).send({ error: baseMessage + " password" })
  }
  // check if password confirmation match
  if (req.body.password !== req.body.passwordConfirmation) {
    return res.status(422).send({ error: "Password confirmation did not match" });
  }
  // check if email is  valid
  // this regex is from https://www.emailregex.com/
  if (!/^(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/.test(req.body.email)) {
    return res.status(400).send({ error: baseMessage + " email" })
  }
  next();
}
function nada() {
  return 1;
}
module.exports = { validateUserBody, nada };
